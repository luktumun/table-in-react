import "./styles.css";
import Xtable from "./Xtable";

export default function App() {
  return (
    <div className="App">
      <Xtable />
    </div>
  );
}

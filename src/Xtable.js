import React from "react";
import { useState, useRef } from "react";

const Xtable = () => {
  var tableData3 = [
    { date: "2022-09-01", views: 100, article: "Article 1" },

    { date: "2023-09-01", views: 100, article: "Article 1" },

    { date: "2023-09-02", views: 150, article: "Article 2" },

    { date: "2023-09-02", views: 120, article: "Article 3" },

    { date: "2020-09-03", views: 200, article: "Article 4" },
  ];

  var tableData1 = [
    { date: "2020-09-03", views: 200, article: "Article 4" },

    { date: "2023-09-02", views: 150, article: "Article 2" },

    { date: "2023-09-02", views: 120, article: "Article 3" },

    { date: "2022-09-01", views: 100, article: "Article 1" },

    { date: "2023-09-01", views: 100, article: "Article 1" },
  ];

  var tableData2 = [
    { date: "2023-09-02", views: 150, article: "Article 2" },

    { date: "2023-09-02", views: 120, article: "Article 3" },

    { date: "2023-09-01", views: 100, article: "Article 1" },

    { date: "2022-09-01", views: 100, article: "Article 1" },

    { date: "2020-09-03", views: 200, article: "Article 4" },
  ];
  const [tableData, setTableData] = useState(tableData3);

  //var sortedByViews = tableData1.sort((a, b) => b.views - a.views); // function call
  var sortedByDate = tableData1.sort((a, b) => b.date - a.date);

  function onClick(event) {
    setTableData(tableData1);
  }
  function onClick1(event) {
    setTableData(tableData2);
  }
  console.log(sortedByDate);
  return (
    <>
	   <h1>Date and Views Table</h1>
      <nav>
        <button onClick={onClick1}>Sort by Date</button>
        <button onClick={onClick}>Sort by Views</button>
        <table>
          <tr>
            <th>Date</th>
            <th>Views</th>
            <th>Article</th>
          </tr>
          {tableData &&
            tableData.map((itm) => (
              <tr key={itm}>
                <td>{itm.date}</td>
                <td>{itm.views}</td>
                <td>{itm.article}</td>
              </tr>
            ))}
        </table>{" "}
      </nav>
    </>
  );
};
export default Xtable;
